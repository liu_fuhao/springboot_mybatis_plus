/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80017 (8.0.17)
 Source Host           : localhost:3306
 Source Schema         : springboot-test-db

 Target Server Type    : MySQL
 Target Server Version : 80017 (8.0.17)
 File Encoding         : 65001

 Date: 19/11/2022 14:33:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_user
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `deleted` int(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '是否删除--0未删除--1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
INSERT INTO `tbl_user` VALUES (1, 'Jone', 18, 'test1@baomidou.com', 0);
INSERT INTO `tbl_user` VALUES (2, 'Jack', 20, 'test2@baomidou.com', 0);
INSERT INTO `tbl_user` VALUES (3, 'Tom', 28, 'test3@baomidou.com', 0);
INSERT INTO `tbl_user` VALUES (4, 'Sandy', 21, 'test4@baomidou.com', 0);
INSERT INTO `tbl_user` VALUES (5, 'Billie', 24, 'test5@baomidou.com', 0);
INSERT INTO `tbl_user` VALUES (6, '张三', 23, 'zhangsan@qq.com', 0);
INSERT INTO `tbl_user` VALUES (7, '李四', 24, 'lisi@qq.com', 0);
INSERT INTO `tbl_user` VALUES (8, 'java', 18, 'java@qq.com', 0);

SET FOREIGN_KEY_CHECKS = 1;
