package com.example.springbootmybatisplus.controller;

import com.example.springbootmybatisplus.bean.User;
import com.example.springbootmybatisplus.service.UserService;
import com.example.springbootmybatisplus.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lfh
 * @since 2022-11-19
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService UserService;

    /**
     * 根据ID查询用户信息
     * @param id
     * @return
     */
    @GetMapping("/getOneUser/{id}")
    public User getOneUser(@PathVariable("id") Integer id){
        return UserService.getById(id);
    }

    /**
     * 获取所有的用户信息
     * @return
     */
    @GetMapping("/getAllUser")
    public List<User> getAllUser(){
        return UserService.list();
    }

}
